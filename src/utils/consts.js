export const projectOptions = [
  { value: "Main DB", label: "Main DB" },
  { value: "Search Engine", label: "Search Engine" },
  { value: "New UX", label: "New UX" },
  { value: "Third Party Components", label: "Third Party Components" }
];

export const statusOptions = [
  { value: "IN PROGRESS", label: "IN PROGRESS" },
  { value: "OPEN", label: "OPEN" },
  { value: "REOPENED", label: "REOPENED" },
  { value: "DONE", label: "DONE" },
  { value: "IN QA", label: "IN QA" },
  { value: "IN REVIEW", label: "IN REVIEW" },
  { value: "WAITING PR", label: "WAITING PR" }
];

export const typeOptions = [
  { value: "Bug", label: "Bug" },
  { value: "Feature", label: "Feature" },
  { value: "Update", label: "Update" }
];

export const assigneeOptions = [
  { value: "John", label: "John" },
  { value: "Raul", label: "Raul" },
  { value: "Mary", label: "Mary" },
  { value: "Alice", label: "Alice" },
  { value: "Anna", label: "Anna" }
];
