const baseCall = "GET /tickets?tql=";

export function createKeywordsGroup(stringArr) {
  let arr = "(";

  stringArr.forEach((element, index, array) => {
    if (index === array.length - 1) {
      arr = arr + `${element})`;
    } else arr = arr + `${element}, `;
  });
  console.log(arr);
  return arr;
}

export function generateQuery({ projects, types, statuses, assignees, term }) {
  let query = baseCall;
  let queryArr = [];

  if (projects.length > 0) {
    const projectsGroup = createKeywordsGroup(projects);

    const projectsQuery = `project IN ${projectsGroup}`;

    queryArr.push(projectsQuery);
  }

  if (types.length > 0) {
    const typesGroup = createKeywordsGroup(types);

    const typesQuery = `type IN ${typesGroup}`;

    queryArr.push(typesQuery);
  }

  if (statuses.length > 0) {
    const statusesGroup = createKeywordsGroup(statuses);

    const statusesQuery = `status IN ${statusesGroup}`;

    queryArr.push(statusesQuery);
  }

  if (assignees.length > 0) {
    const assigneesGroup = createKeywordsGroup(assignees);

    const assigneesQuery = `assignee IN ${assigneesGroup}`;

    queryArr.push(assigneesQuery);
  }

  queryArr.forEach((element, index, array) => {
    if (index === array.length - 1) {
      query = query + `${element}`;
    } else query = query + `${element} AND `;
  });

  if (term !== "") {
    query = query + ` AND term ~ "${term}"`;
  }

  return query;
}
