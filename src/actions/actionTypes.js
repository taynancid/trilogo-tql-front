//project
export const UPDATE_PROJECT = "UPDATE_PROJECT";

//type
export const UPDATE_TYPE = "UPDATE_TYPE";

//status
export const UPDATE_STATUS = "UPDATE_STATUS";

//assignee
export const UPDATE_ASIGNEE = "UPDATE_ASIGNEE";

//term
export const EDIT_TERM = "EDIT_TERM";

//query string
export const UPDATE_STRING = "UPDATE_STRING";
