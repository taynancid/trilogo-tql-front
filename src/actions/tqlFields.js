import * as actionTypes from "./actionTypes";

//project
export function updateProjects(projects) {
  return {
    type: actionTypes.UPDATE_PROJECT,
    projects
  };
}

//type
export function updateTypes(types) {
  return {
    type: actionTypes.UPDATE_TYPE,
    types
  };
}

//status
export function updateStatuses(statuses) {
  return {
    type: actionTypes.UPDATE_STATUS,
    statuses
  };
}

//assignee
export function updateAssignees(assignees) {
  return {
    type: actionTypes.UPDATE_ASIGNEE,
    assignees
  };
}

//term
export function editTerm(term) {
  return {
    type: actionTypes.EDIT_TERM,
    term
  };
}
