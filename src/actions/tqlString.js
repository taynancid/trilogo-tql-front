import * as actionTypes from "./actionTypes";

//project
export function updateString(string) {
  return {
    type: actionTypes.UPDATE_STRING,
    string: string
  };
}
