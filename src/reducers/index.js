import { combineReducers } from "redux";
import tqlFields from "./tqlFields";
import tqlString from "./tqlString";

export default combineReducers({
  tqlFields,
  tqlString
});
