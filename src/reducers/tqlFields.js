import * as actionTypes from "../actions/actionTypes";

//state structure:
const initialState = {
  projects: [],
  types: [],
  statuses: [],
  assignees: [],
  term: ""
};

export default function tqlFields(state = initialState, action) {
  switch (action.type) {
    case actionTypes.UPDATE_PROJECT:
      return {
        ...state,
        projects: action.projects
      };
    case actionTypes.UPDATE_TYPE:
      return {
        ...state,
        types: action.types
      };
    case actionTypes.UPDATE_STATUS:
      return {
        ...state,
        statuses: action.statuses
      };
    case actionTypes.UPDATE_ASIGNEE:
      return {
        ...state,
        assignees: action.assignees
      };
    case actionTypes.EDIT_TERM:
      return {
        ...state,
        term: action.term
      };
    default:
      return state;
  }
}
