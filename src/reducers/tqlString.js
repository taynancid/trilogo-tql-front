import * as actionTypes from "../actions/actionTypes";

export default function tqlString(state = { query: "" }, action) {
  switch (action.type) {
    case actionTypes.UPDATE_STRING:
      return {
        query: action.string
      };
    default:
      return state;
  }
}
