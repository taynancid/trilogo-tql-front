# Trilogo - Frontend Developer Challenge

## Trilogo Query Language (TQL)

This challenge was made with React/Redux.

The Redux store has 2 reducers:
1 - the fields reducer, which state has all the fields in their current value, according with the UI
2 - the query reducer, which state has only the last query searched

### Run the app

is necessary to have yarn or npm to run this project

in the root folder, run:
1 - yarn install, to install dependencies
2 - yarn run, to run it

### Test the app (not done)

after install the dependencies run
1 - yarn test
