import React, { Component } from "react";
import Select from "react-select";
import {
  updateProjects,
  editTerm,
  updateTypes,
  updateAssignees,
  updateStatuses
} from "../actions/tqlFields";
import {
  projectOptions,
  statusOptions,
  typeOptions,
  assigneeOptions
} from "../utils/consts";
import { updateString } from "../actions/tqlString";
import { connect } from "react-redux";
import "./App.css";
import { generateQuery } from "../utils/helpers";

class App extends Component {
  handleChange = (type, options) => {
    const { dispatch } = this.props;
    const values = options.map(option => option.value);

    if (type === "project") dispatch(updateProjects(values));
    else if (type === "type") dispatch(updateTypes(values));
    else if (type === "status") dispatch(updateStatuses(values));
    else if (type === "assignee") dispatch(updateAssignees(values));
  };

  handleTerm = e => {
    const { dispatch } = this.props;
    const term = e.target.value;
    dispatch(editTerm(term));
  };

  generateQuery = e => {
    const { dispatch } = this.props;
    const query = generateQuery(this.props.fields);
    dispatch(updateString(query));
  };

  render() {
    return (
      <div className="container">
        <div className="nav-container">
          <Select
            className="select"
            placeholder={"Project: All"}
            onChange={option => this.handleChange("project", option)}
            options={projectOptions}
            isMulti={true}
            isSearchable={true}
          />

          <Select
            className="select"
            placeholder={"Type: All"}
            onChange={option => this.handleChange("type", option)}
            options={typeOptions}
            isMulti={true}
            isSearchable={true}
          />

          <Select
            className="select"
            placeholder={"Status: All"}
            onChange={option => this.handleChange("status", option)}
            options={statusOptions}
            isMulti={true}
            isSearchable={true}
          />

          <Select
            className="select"
            placeholder={"Assignee: All"}
            onChange={option => this.handleChange("assignee", option)}
            options={assigneeOptions}
            isMulti={true}
            isSearchable={true}
          />

          <input
            placeholder="Contains text"
            className="input2"
            type="text"
            onChange={this.handleTerm}
          />

          <div className="btn" onClick={this.generateQuery}>
            <i className="fas fa-search" />
          </div>
        </div>
        <span className="queryDisplay">{this.props.query}</span>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    fields: state.tqlFields,
    query: state.tqlString.query
  };
}

export default connect(mapStateToProps)(App);
